import React, { useState } from 'react';


export default function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })

  const handleSubmit = async (event) => {
    event.preventDefault();

    const customerUrl = 'http://localhost:8090/api/customers/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: '',
      });
    }
  }


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="First_name" value={formData.first_name} required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="fabric">First name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Last_name" value={formData.last_name} required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="style_name">Last name</label>
            </div>

             <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Phone_number" required type="text" value={formData.phone_number} name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="color">Phone number</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Address" required type="text" value={formData.address} name="address" id="address" className="form-control" />
              <label htmlFor="color">Address</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
