import React, { useEffect, useState } from "react";


export default function NewSaleForm() {
  const [autos, setAutos] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [auto, setAuto] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");

  const handleAutoChange = (event) => {
    const value = event.target.value;
    setAuto(value);
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.auto = auto;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const saleUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
      setAuto("");
      setSalesperson("");
      setCustomer("");
      setPrice("");
    }
  };

  const fetchData = async () => {
    const [autoResponse, salespeopleResponse, customerResponse] =
      await Promise.all([
        fetch("http://localhost:8100/api/automobiles/"),
        fetch("http://localhost:8090/api/salespeople/"),
        fetch("http://localhost:8090/api/customers/"),
      ]);

    if (autoResponse.ok || salespeopleResponse.ok || customerResponse.ok) {
      const [autoData, salespeopleData, customerData] = await Promise.all([
        autoResponse.json(),
        salespeopleResponse.json(),
        customerResponse.json(),
      ]);
      setAutos(autoData.autos);
      setSalespeople(salespeopleData.salespeople);
      setCustomers(customerData.customers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="form-floating mb-3">
                <select
                  onChange={handleAutoChange}
                  placeholder="Automobile"
                  value={auto}
                  required
                  name="automobile"
                  id="automobile"
                  className="form-select"
                >
                  <label htmlFor="automobile">Automobile VIN</label>
                  <option value="">Choose an automobile VIN...</option>
                  {autos.map((auto) => {
                    return (
                      <option key={auto.vin} value={auto.vin}>
                        {auto.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <select
                  onChange={handleSalespersonChange}
                  placeholder="Salesperson"
                  value={salesperson}
                  required
                  name="salesperson"
                  id="salesperson"
                  className="form-select"
                >
                  <label htmlFor="salesperson">Salesperson</label>
                  <option value="">Choose a salesperson...</option>
                  {salespeople.map((salesperson) => {
                    return (
                      <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <select
                  onChange={handleCustomerChange}
                  placeholder="Customer"
                  value={customer}
                  required
                  name="customer"
                  id="customer"
                  className="form-select"
                >
                  <label htmlFor="customer">Customer</label>
                  <option value="">Choose a Customer...</option>
                  {customers.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handlePriceChange}
                  placeholder="Price"
                  value={price}
                  required
                  type="number"
                  name="price"
                  id="price"
                  className="form-control"
                />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
